﻿using LandscapeBot1017.GauGAN;
using LandscapeBot1017.LandscapeObjects;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace LandscapeBot1017 {
	public class Landscape {
		public SortedDictionary<int, LandscapeObject> DepthMap;
		public List<int> ExistingDepths;

		public Landscape() {
			// setup basic stuff for a landscape
			DepthMap = new SortedDictionary<int, LandscapeObject>();
			ExistingDepths = new List<int>();
		}

		public void Add(int depth, LandscapeObject obj) {
			while (ExistingDepths.IndexOf(depth) >= 0) {
				depth++;
			}
			ExistingDepths.Add(depth);
			DepthMap.Add(depth, obj);
		}
	}
}
