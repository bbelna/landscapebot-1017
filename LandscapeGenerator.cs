﻿using LandscapeBot1017.GauGAN;
using LandscapeBot1017.SceneProfiles;
using System;
using System.Collections.Generic;
using System.Text;

namespace LandscapeBot1017 {
	public class LandscapeGenerator {
		public Landscape Generate() {
			// TODO: add random selection of scene profiles
			SceneProfile sceneProfile = new MountainLandscape();
			// first decide what kind of scene this is going to be...
			Landscape landscape = sceneProfile.GenerateLandscape();
			return landscape;
		}
	}
}
