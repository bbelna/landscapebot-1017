﻿using Facebook;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Dynamic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace LandscapeBot1017.Facebook {
	public static class FacebookInterface {
		public static string AccessToken = Configuration.InitialToken;

		public static string GetPageAccessToken(string userAccessToken) {
			FacebookClient fbClient = new FacebookClient();
			fbClient.AppId = Configuration.AppID;
			fbClient.AppSecret = Configuration.AppSecret;
			fbClient.AccessToken = userAccessToken;
			Dictionary<string, object> fbParams = new Dictionary<string, object>();
			JsonObject publishedResponse = fbClient.Get("/me/accounts", fbParams) as JsonObject;
			JArray data = JArray.Parse(publishedResponse["data"].ToString());

			foreach (var account in data) {
				if (account["name"].ToString().ToLower().Equals("LandscapeBot 1017")) {
					return account["access_token"].ToString();
				}
			}

			return String.Empty;
		}

		public static string RefreshAccessToken(string currentAccessToken) {
			FacebookClient fbClient = new FacebookClient();
			Dictionary<string, object> fbParams = new Dictionary<string, object>();
			fbParams["client_id"] = Configuration.AppID;
			fbParams["grant_type"] = "fb_exchange_token";
			fbParams["client_secret"] = Configuration.AppSecret;
			fbParams["fb_exchange_token"] = currentAccessToken;
			JsonObject publishedResponse = fbClient.Get("/oauth/access_token", fbParams) as JsonObject;
			return publishedResponse["access_token"].ToString();
		}

		public static void UploadImage(Stream imageStream) {
			WebClient client = new WebClient();
			client.UploadFile("https://graph.facebook.com/me/photos?access_token=" + AccessToken, "POST", "Output.jpeg");
		}

		public static byte[] ReadFully(Stream input) {
			using (MemoryStream ms = new MemoryStream()) {
				input.CopyTo(ms);
				return ms.ToArray();
			}
		}
	}
}
