﻿using LandscapeBot1017.GauGAN;
using LandscapeBot1017.LandscapeObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace LandscapeBot1017.SceneProfiles {
	public class Seascape : SceneProfile {
		public Seascape() {
			Backgrounds = new LandscapeObject[] {
				new Sky(),
				null,
				new Ocean() {
					Dimensions = new int[] { 512, 170 }
				}
			};
			Maximums = new int[] { 8, 10, 10 };
			ObjectTemplates = new LandscapeObject[][] {
				new LandscapeObject[] { new Cloud() },
				new LandscapeObject[] { new Hill() },
				new LandscapeObject[] { new Sand(), new Rock() }
			};
		}
	}
}
