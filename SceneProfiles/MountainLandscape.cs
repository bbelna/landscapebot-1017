﻿using LandscapeBot1017.GauGAN;
using LandscapeBot1017.LandscapeObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace LandscapeBot1017.SceneProfiles {
	public class MountainLandscape : SceneProfile {
		
		public MountainLandscape() {
			Backgrounds = new LandscapeObject[] {
				new Sky(),
				null,
				new Mountain() {
					Dimensions = new int[] { 512, 170 }
				}
			};
			Maximums = new int[] { 8, 10, 5 };
			ObjectTemplates = new LandscapeObject[][] {
				new LandscapeObject[] { new Cloud() },
				new LandscapeObject[] { new Mountain() },
				new LandscapeObject[] { new Mountain() }
			};
		}
	}
}
