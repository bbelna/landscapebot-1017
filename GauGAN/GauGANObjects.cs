﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace LandscapeBot1017.GauGAN {
	public static class GauGANObjects {
		public static Color Sky = Color.FromArgb(156, 238, 221);
		public static Color Mountain = Color.FromArgb(134, 150, 100);
		public static Color Grass = Color.FromArgb(123, 200, 0);
		public static Color Sand = Color.FromArgb(153, 153, 0);
		public static Color Rock = Color.FromArgb(161, 161, 100);
		public static Color Bush = Color.FromArgb(96, 110, 50);
		public static Color Dirt = Color.FromArgb(110, 110, 40);
		public static Color River = Color.FromArgb(147, 100, 200);
		public static Color Cloud = Color.FromArgb(105, 105, 105);
		public static Color Tree = Color.FromArgb(168, 200, 50);
		public static Color Sea = Color.FromArgb(154, 198, 218);
		public static Color Hill = Color.FromArgb(126, 200, 100);
	}
}
