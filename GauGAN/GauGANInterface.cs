﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace LandscapeBot1017.GauGAN {
	public class GauGANInterface {
		public async Task UploadImage(Bitmap bitmap) {
			// save the bitmap in memory as a PNG
			System.IO.MemoryStream stream = new System.IO.MemoryStream();
			bitmap.Save(stream, System.Drawing.Imaging.ImageFormat.Png);

			// get the byte stream
			byte[] imageBytes = stream.ToArray();

			// Convert byte[] to Base64 String
			string base64String = Convert.ToBase64String(imageBytes);

			// setup file name
			string fileName = "12/8/2019,1575816781077-515607955";

			// setup request body
			GauGANImageGenerationRequestModel requestModel = new GauGANImageGenerationRequestModel(base64String, fileName);
			var nvc = new List<KeyValuePair<string, string>>();
			nvc.Add(new KeyValuePair<string, string>("imageBase64", "data:image/png;base64," + requestModel.imageBase64));
			nvc.Add(new KeyValuePair<string, string>("name", requestModel.name));
			var req = new HttpRequestMessage(HttpMethod.Post, "http://34.221.84.127:443/nvidia_gaugan_submit_map") { Content = new FormUrlEncodedContent(nvc) };

			// perform request to GauGAN
			var client = new HttpClient();
			await client.SendAsync(req);
		}

		public Stream ReceiveImage() {
			// https://stackoverflow.com/questions/3735988/how-to-post-raw-data-using-c-sharp-httpwebrequest
			HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://34.221.84.127:443/nvidia_gaugan_receive_image");
			request.ContentType = "multipart/form-data; boundary=---------------------------30049338930732";
			request.Method = "POST";
			StreamWriter requestWriter = new StreamWriter(request.GetRequestStream());

			try {
				requestWriter.Write("-----------------------------30049338930732\nContent-Disposition: form-data; name = \"name\"\n\n12/8/2019,1575816781077-515607955\n" +
					"-----------------------------30049338930732\nContent-Disposition: form-data; name=\"style_name\"\n\nrandom\n" +
					"-----------------------------30049338930732--");
			} catch {
				throw;
			} finally {
				requestWriter.Close();
			}

			HttpWebResponse response = (HttpWebResponse)request.GetResponse();
			return response.GetResponseStream();
		}
	}
}
