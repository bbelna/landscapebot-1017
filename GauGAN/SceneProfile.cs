﻿using LandscapeBot1017.LandscapeObjects;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace LandscapeBot1017.GauGAN {
	public abstract class SceneProfile {
		protected LandscapeObject[] Backgrounds;

		protected LandscapeObject[][] ObjectTemplates;

		protected int[] Maximums;

		protected int[] Borders = new int[3] { 171, 342, 512 };

		public Landscape GenerateLandscape() {
			Landscape landscape = new Landscape();
			// setup background
			Random random = new Random();
			for (int i = 0; i < Maximums.Length; i++) {
				// setup the background for this layer
				if (Backgrounds[i] != null) {
					Backgrounds[i].Position[1] = i > 0 ? Borders[i - 1] : 0;
					landscape.Add(i * 100, Backgrounds[i]);
				}
				for (int j = 0; j < random.Next(Maximums[i]); j++) {
					// randomly pick the object to add
					int k = random.Next(ObjectTemplates[i].Length);
					// copy the template object
					LandscapeObject ObjectToAdd = ObjectTemplates[i][k].ShallowCopy();
					// randomize its position in accordance to our borders
					int verticalOffset = i > 0 ? Borders[i - 1] : 0;
					if (!ObjectToAdd.InvertPositioning) {
						ObjectToAdd.Position = new int[] {
							random.Next(Math.Max(512 - ObjectToAdd.Dimensions[0], 0)),
							verticalOffset + random.Next(Math.Max(Borders[i] - ObjectToAdd.Dimensions[1], 0))
						};
					} else {
						if (ObjectToAdd.IgnoreBounds) {
							ObjectToAdd.Position = new int[] {
								random.Next(Math.Max(512 - ObjectToAdd.Dimensions[0], 0)),
								// TODO: refactor
								(i > 0 ? Borders[i - 1] : 0) + random.Next(170)
							};
						} else {
							ObjectToAdd.Position = new int[] {
								random.Next(Math.Max(512 - ObjectToAdd.Dimensions[0], 0)),
								verticalOffset - ObjectToAdd.Dimensions[1] + random.Next(Math.Max(Borders[i] - ObjectToAdd.Dimensions[1], 0))
							};
						}
					}
					
					// determine its depth
					int depth = 1 + i*100 + j;
					// and add it to the scene
					landscape.Add(depth, ObjectToAdd);
				}
			}
			
			return landscape;
		}
	}
}
