﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LandscapeBot1017.GauGAN {
	public class GauGANImageGenerationRequestModel {
		public string imageBase64;
		public string name;

		public GauGANImageGenerationRequestModel(string _imageBase64, string _name) {
			imageBase64 = _imageBase64;
			name = _name;
		}
	}
}
