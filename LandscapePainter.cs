﻿using LandscapeBot1017.LandscapeObjects;
using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

namespace LandscapeBot1017 {
	public class LandscapePainter {
		public Bitmap Paint(Landscape landscape) {
			Bitmap bitmap = new Bitmap(512, 512);
			int i = 0;
			// iterate over the landscape's depth map and draw the objects
			foreach (KeyValuePair<int, LandscapeObject> landscapeObject in landscape.DepthMap) {
				using (Graphics g = Graphics.FromImage(bitmap)) {
					g.SmoothingMode = SmoothingMode.None;
					// draw the object!
					int[] objectPos = landscapeObject.Value.Position;
					int[] objectDim = landscapeObject.Value.Dimensions;
					Color objectCol = landscapeObject.Value.Color;
					SolidBrush brush = new SolidBrush(objectCol);
					Rectangle rect = new Rectangle(objectPos[0], objectPos[1], objectDim[0], objectDim[1]);
					if (!landscapeObject.Value.AmEllipse) {
						g.FillRectangle(brush, rect);
					} else {
						g.FillEllipse(brush, rect);
					}
					if (Program.Debug) {
						Image inputImg = bitmap;
						inputImg.Save(i + ".jpeg", ImageFormat.Jpeg);
						i++;
					}
					brush.Dispose();
					g.Dispose();
				}
			}
			
			return bitmap;
		}
	}
}
