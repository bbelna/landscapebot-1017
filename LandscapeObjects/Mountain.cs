﻿using LandscapeBot1017.GauGAN;
using System;
using System.Collections.Generic;
using System.Text;

namespace LandscapeBot1017.LandscapeObjects {
	public class Mountain : LandscapeObject {
		public Mountain() {
			Random random = new Random();
			Dimensions = new int[] { 512, 100 + random.Next(100) };
			Color = GauGANObjects.Mountain;
			Position = new int[] { 0, 0 };
		}
	}
}
