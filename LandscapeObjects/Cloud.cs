﻿using LandscapeBot1017.GauGAN;
using System;
using System.Collections.Generic;
using System.Text;

namespace LandscapeBot1017.LandscapeObjects {
	class Cloud : LandscapeObject {
		public Cloud() {
			Random random = new Random();
			Dimensions = new int[] { 200 + random.Next(100), 100 + random.Next(50) };
			Color = GauGANObjects.Cloud;
			Position = new int[] { 0, 0 };
		}
	}
}
