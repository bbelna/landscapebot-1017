﻿using LandscapeBot1017.GauGAN;
using System;
using System.Collections.Generic;
using System.Text;

namespace LandscapeBot1017.LandscapeObjects {
	public class Rock : LandscapeObject {
		public Rock() {
			Random random = new Random();
			Dimensions = new int[] { 25 + random.Next(100), 50 + random.Next(150) };
			Color = GauGANObjects.Rock;
			Position = new int[] { 0, 0 };
			InvertPositioning = true;
		}
	}
}
