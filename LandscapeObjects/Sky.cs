﻿using LandscapeBot1017.GauGAN;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace LandscapeBot1017.LandscapeObjects {
	public class Sky : LandscapeObject {
		public Sky() {
			Dimensions = new int[] { 512, 512 };
			Color = GauGANObjects.Sky;
			Position = new int[] { 0, 0 };
		}
	}
}
