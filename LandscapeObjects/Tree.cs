﻿using LandscapeBot1017.GauGAN;
using System;
using System.Collections.Generic;
using System.Text;

namespace LandscapeBot1017.LandscapeObjects {
	public class Tree : LandscapeObject {
		public Tree() {
			Random random = new Random();
			Dimensions = new int[] { 30 + random.Next(40), 100 + random.Next(200) };
			InvertPositioning = true;
			Color = GauGANObjects.Tree;
			Position = new int[] { 0, 0 };
		}
	}
}
