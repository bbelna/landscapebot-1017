﻿using LandscapeBot1017.GauGAN;
using System;
using System.Collections.Generic;
using System.Text;

namespace LandscapeBot1017.LandscapeObjects {
	public class Grass : LandscapeObject {
		public Grass() {
			Random random = new Random();
			Dimensions = new int[] { 512, 170 };
			Color = GauGANObjects.Grass;
			Position = new int[] { 0, 0 };
		}
	}
}
