﻿using LandscapeBot1017.GauGAN;
using System;
using System.Collections.Generic;
using System.Text;

namespace LandscapeBot1017.LandscapeObjects {
	public class Sand : LandscapeObject {
		public Sand() {
			Random random = new Random();
			Dimensions = new int[] { 512, 10 + random.Next(100)};
			Color = GauGANObjects.Sand;
			Position = new int[] { 0, 0 };
		}
	}
}
