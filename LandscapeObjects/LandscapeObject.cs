﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace LandscapeBot1017.LandscapeObjects {
	public abstract class LandscapeObject {
		public int[] Dimensions;
		public Color Color;
		public int[] Position;
		public bool InvertPositioning = false;
		// will refactor later, but for now...
		public bool AmEllipse = false;
		public bool IgnoreBounds = false;

		public LandscapeObject ShallowCopy() {
			return (LandscapeObject) this.MemberwiseClone();
		}
	}
}
