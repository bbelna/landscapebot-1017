﻿using LandscapeBot1017.GauGAN;
using System;
using System.Collections.Generic;
using System.Text;

namespace LandscapeBot1017.LandscapeObjects {
	public class Hill : LandscapeObject {
		public Hill() {
			Random random = new Random();
			Dimensions = new int[] { 200 + random.Next(300), 25 + random.Next(100)};
			Color = GauGANObjects.Hill;
			Position = new int[] { 0, 0 };
			AmEllipse = true;
			IgnoreBounds = true;
		}
	}
}
