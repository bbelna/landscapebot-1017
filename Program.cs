﻿using LandscapeBot1017.Facebook;
using LandscapeBot1017.GauGAN;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace LandscapeBot1017 {
	public static class Program {
		public static Boolean Debug = true;

		static void Main(string[] args) {
			Console.WriteLine("LANDSCAPEBOT 1017");
			Console.WriteLine("*****************************************");
			// initial setup
			Console.WriteLine("Initializing...");
			LandscapeGenerator generator = new LandscapeGenerator();
			LandscapePainter painter = new LandscapePainter();
			GauGANInterface gauGANInterface = new GauGANInterface();
			// generate the landscape
			Console.WriteLine("Generating a landscape...");
			Landscape landscape = generator.Generate();
			// create an image file
			Console.WriteLine("Painting the landscape...");
			Bitmap bitmap = painter.Paint(landscape);
			// upload to NVIDIA GauGAN, receive image back
			Console.WriteLine("Uploading to GauGAN...");
			gauGANInterface.UploadImage(bitmap).Wait();
			Console.WriteLine("Success! Fetching image from GauGAN...");
			Stream imageStream = gauGANInterface.ReceiveImage();
			Console.WriteLine("Writing files...");
			Image outputImg = Image.FromStream(imageStream);
			Image inputImg = bitmap;
			inputImg.Save("Input.jpeg", ImageFormat.Jpeg);
			outputImg.Save("Output.jpeg", ImageFormat.Jpeg);
			// upload to Facebook
			Console.WriteLine("Uploading to Facebook...");
			FacebookInterface.RefreshAccessToken(Configuration.InitialToken);
			FacebookInterface.UploadImage(imageStream);
			// done
			Console.WriteLine("Finished!");
		}
	}
}
